"""This Module provides access to the mathematical functions defined by the C standard."""
import math

def is_prime(number):
    """A function for primary number """
    if number < 0:
        return 'Negative numbers are not allowed'
    if number <= 1:
        return False
    if number == 2:
        return True
    if number % 2 == 0:
        return False
    for i in range(2, int(math.sqrt(number)) + 1):
        if number % i == 0:
            return False
    return True

def cubic(number):
    """A function for cube calcul"""
    return number * number * number

def say_hello(name):
    """A function to say hello"""
    return "Hello, " + name

say_hello("world !")
